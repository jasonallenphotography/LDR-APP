## As a new user:
  * I want to be able to register using my Facebook account
  * I want to be able to optionally use one of my profile photos for my avatar (or a dummy picture provided)
  * I want to be asked what distances I run from a short list (choose all that apply)
  * I want to be able to enter my pace info for each distance I chose
  * I want to be able to play with the site at that point

## As an authenticated user I want to be able to:
  * Find existing friends via FB friends
  * Find new running buddies with similar pace and goals
  * Save custom shout-outs to a list that only I see
  * Select a short list of 9 (3x3) selected shoutouts per friends

## When I go on a run, I want to:
  ### Be able to start a 'run session' with a remote friend
    * Know my accurate pace and distance
    * Receive updates every 15-20 seconds on my friend's pace and distance
    * Be able to send customized shout-outs saved for that friend relation
  ### Be able to take a picture
    * Tag the picture with what mile on your run you're at OR location that you're near
    * share the picture with your remote friend during a 'run session'
    * save the picture for later upload to IG or FB