class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      # OAuth group
      t.string    :provider
      t.string    :uid
      t.string    :name
      t.string    :oauth_token
      t.datetime  :oauth_expires_at

      # Profile information for LDR
      t.string    :email
      t.text      :about
      t.string    :picture_url

      t.timestamps null: false
    end
  end
end
